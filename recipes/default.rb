file "/etc/apt/apt.conf.d/04options" do
    content %Q{APT::Install-Recommends "false";\n}
    mode "0644"
end

package "squid-deb-proxy-client" do
    action :install
end

apt_update 'daily apt update' do
    frequency 86_400
    action :periodic
end

package "flashplugin-installer" do
    action :purge
end

package "tmux"

# if node.role?(:laptop)
# package "acpi"
# package "xbacklight"

include_recipe "pasokon::localdirs"
include_recipe "pasokon::gui"
include_recipe "pasokon::japanese"

include_recipe "vim"
