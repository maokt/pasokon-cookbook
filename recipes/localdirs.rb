%w{bin etc include lib share src}.each do |n|
    directory "/usr/local/#{n}" do
        owner "root"
        group "staff"
        mode "0775"
        action :create
    end
end
