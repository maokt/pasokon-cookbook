name 'pasokon'
maintainer 'Marty Pauley'
maintainer_email 'marty+chef@martian.org'
license 'apachev2'
description 'configuration for my personal computers'
long_description 'mostly just a list of packages to install'
version '0.1.0'
depends "vim"
